﻿namespace dotnet_code_challenge.Enums
{
    public enum HorseOrderByEnum
    {
        NameAscending,
        PriceAscending
    }
}