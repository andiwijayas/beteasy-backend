﻿using System;

namespace dotnet_code_challenge.Exceptions
{
    public class InvalidFileException : Exception
    {
        public InvalidFileException(string message) : base(message)
        {
        }
    }
}
