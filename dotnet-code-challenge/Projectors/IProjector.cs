﻿using System.Collections.Generic;

namespace dotnet_code_challenge.Projectors
{
    public interface IProjector<T> where T:class
    {
        IEnumerable<T> GetProjection(IEnumerable<T> source);
    }
}