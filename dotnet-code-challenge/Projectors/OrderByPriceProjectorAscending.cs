﻿using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Models;

namespace dotnet_code_challenge.Projectors
{
    public class OrderByPriceProjectorAscending : IProjector<Horse>
    {
        public IEnumerable<Horse> GetProjection(IEnumerable<Horse> source)
        {
            return source.OrderBy(x => x.Price);
        }
    }
}