﻿using System;
using System.Collections.Generic;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Services;
using Microsoft.Extensions.Logging;

namespace dotnet_code_challenge
{
    public class App
    {
        private readonly IHorseService _horseService;
        private readonly ILogger<App> _logger;

        public App(IHorseService horseService, ILogger<App> logger)
        {
            _horseService = horseService;
            _logger = logger;
        }

        public void Run()
        {
            _logger.LogInformation("Starting App");
            try {
                Console.Clear();
                var horses = _horseService.GetHorses(
                                    new List<HorseOrderByEnum>
                                    {
                                        HorseOrderByEnum.PriceAscending
                                    });
                
                foreach (var horse in horses)
                {
                    Console.WriteLine($"Name: {horse.Name} - Price: ${horse.Price} - Race: {horse.RaceName}");
                }
            } 
            catch (Exception ex) 
            {
                _logger.LogError(ex, "Error While running App");
                Console.WriteLine(ex.Message);
            }
            _logger.LogInformation("End APp");            
            
        }
    }
}