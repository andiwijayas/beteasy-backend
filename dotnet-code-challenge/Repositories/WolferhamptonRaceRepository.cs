﻿using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Infrastructures;
using dotnet_code_challenge.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace dotnet_code_challenge.Repositories
{
    public class WolferhamptonRaceRepository: IRaceRepository<WolferhamptonRace.Meeting>, IHorseRepository
    {
        private Func<string, IFileDeserializer> _deserializerFactory;
        private readonly IOptions<Config> _config;
        private readonly IFileObjectCacher _fileObjectCacher;
        private readonly ILogger<WolferhamptonRaceRepository> _logger;

        public WolferhamptonRaceRepository (
            Func<string, IFileDeserializer> deserializerFactory, 
            IOptions<Config> config,
            IFileObjectCacher fileObjectCacher,
            ILogger<WolferhamptonRaceRepository> logger
        )
        {
            _deserializerFactory = deserializerFactory;
            _config = config;
            _fileObjectCacher = fileObjectCacher;
            _logger = logger;
        }
        
        public WolferhamptonRace.Meeting GetRace()
        {
            var filePath =  _config.Value.WolferhamptonRaceFilePath;             
            var returnValue = _fileObjectCacher.GetCache<WolferhamptonRace.Meeting>(filePath);
            if (returnValue == null) 
            {
                _logger.LogDebug($"Cache returning null, saving cache on {filePath}");
                returnValue = _deserializerFactory(filePath).Deserialize<WolferhamptonRace.Meeting>(filePath);
                _fileObjectCacher.SetCache<WolferhamptonRace.Meeting>(filePath, returnValue);
            } 
            return returnValue;
        }

        public IEnumerable<Horse> GetHorses()
        {
            return GetRace()?
                    .RawData?
                    .Markets?
                    .SelectMany(x => x.Selections)
                    .Select(x =>
                    new Horse
                    {
                        Name = x.Tags.Name, 
                        Price = Convert.ToDecimal(x.Price), 
                        RaceName = Race.Wolferhampton
                    })
                ?? new List<Horse>();
            
        }
    }
}