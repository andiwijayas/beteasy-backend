using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Infrastructures;
using dotnet_code_challenge.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;


namespace dotnet_code_challenge.Repositories
{
    public class CaulfiedRaceRepository : IRaceRepository<CaulfieldRace.Meeting>, IHorseRepository
    {
        private readonly Func<string, IFileDeserializer> _deserializerFactory;
        private readonly IOptions<Config> _config;
        private readonly IFileObjectCacher _fileObjectCacher;
        private readonly ILogger<CaulfiedRaceRepository> _logger;

        public CaulfiedRaceRepository(
            Func<string, IFileDeserializer> deserializerFactory, 
            IOptions<Config> config,
            IFileObjectCacher fileObjectCacher,
            ILogger<CaulfiedRaceRepository> logger
            )
        {
            _deserializerFactory = deserializerFactory;
            _config = config;
            _fileObjectCacher = fileObjectCacher;
            _logger = logger;
        }
        
        public CaulfieldRace.Meeting GetRace()
        {
            var filePath = _config.Value.CaulfieldRaceFilePath;
            var returnValue = _fileObjectCacher.GetCache<CaulfieldRace.Meeting>(filePath);
            if (returnValue == null) 
            {
                _logger.LogDebug($"Cache returning null, saving cache on {filePath}");
                returnValue = _deserializerFactory(filePath).Deserialize<CaulfieldRace.Meeting>(filePath);
                _fileObjectCacher.SetCache<CaulfieldRace.Meeting>(filePath, returnValue);
            } 
            return returnValue;
        }

        public IEnumerable<Horse> GetHorses()
        {
            var race = GetRace();
            
            var horses = race?
                        .Races?
                        .Race?
                        .Horses?
                        .Horse
                ?? new List<CaulfieldRace.Horse>();
            
            return race?
                       .Races?
                       .Race?
                       .Prices?
                       .Price?
                       .Horses?
                       .Horse?
                       .Select(x => new Horse
                       {
                           Name = x.Name ?? horses.FirstOrDefault(y=>y._Number == x.Id).Name,
                           Price = Convert.ToDecimal(x.Price ?? "0"),
                           RaceName = Race.Caulfied
                       }) 
                   ?? new List<Horse>();
        }
    }
}