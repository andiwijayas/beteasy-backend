﻿using System.Collections.Generic;
using dotnet_code_challenge.Models;

namespace dotnet_code_challenge.Repositories
{
    public interface IRaceRepository<T> where T:class
    {
        T GetRace();
    }
}
