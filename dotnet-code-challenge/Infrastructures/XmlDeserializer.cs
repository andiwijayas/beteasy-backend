﻿using System;
using System.IO;
using System.Xml.Serialization;
using dotnet_code_challenge.Exceptions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

namespace dotnet_code_challenge.Infrastructures
{
    public class XmlDeserializer : IXmlDeserializer
    {
        private readonly ILogger<XmlDeserializer> _logger;
        private readonly IFileProvider _fileProvider;

        public XmlDeserializer(ILogger<XmlDeserializer> logger, IFileProvider fileProvider)
        {
            _fileProvider = fileProvider;
            _logger = logger;
        }
        
        public T Deserialize<T>(string filePath)
        {
            if (filePath == null)
            {
                _logger.LogError("Source file not initialized");
                throw new InvalidFileException($"Source file must be initialised first");
            }
            
            var fileInfo = _fileProvider.GetFileInfo(filePath);
            
            if (!fileInfo.Exists)
            {
                _logger.LogError($"File {filePath} not exists");
                throw new InvalidFileException($"Source file doesn't exists");
            }
            else if (!filePath.EndsWith(".xml"))
            {
                _logger.LogError($"File {filePath} doesn't have .xml extension");
                throw new InvalidFileException($"File {filePath} doesn't have .xml extension");           
            }
            try
            {
                var xml = new XmlSerializer(typeof(T));
                var reader = new StreamReader(fileInfo.PhysicalPath);
                return (T) xml.Deserialize(reader);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}