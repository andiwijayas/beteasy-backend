﻿using System.Security.Cryptography.X509Certificates;

namespace dotnet_code_challenge.Infrastructures
{
    public interface IFileDeserializer
    {
        T Deserialize<T>(string filePath);
    }

}