﻿using System;
using System.IO;
using dotnet_code_challenge.Exceptions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace dotnet_code_challenge.Infrastructures {
    public class JsonDeserializer : IJsonDeserializer {
        private readonly ILogger<JsonDeserializer> _logger;
        private readonly IFileProvider _fileProvider;

        public JsonDeserializer (ILogger<JsonDeserializer> logger, IFileProvider fileProvider) {
            _fileProvider = fileProvider;
            _logger = logger;
        }

        public T Deserialize<T> (string filePath) {            
            if (filePath == null) {
                _logger.LogError ("Source file not initialized");
                throw new InvalidFileException ("Source file must be initialised first");
            }

            var fileInfo = _fileProvider.GetFileInfo(filePath);
            
            if (!fileInfo.Exists) {
                _logger.LogError ($"File {filePath} not exists");
                throw new InvalidFileException ($"Source {filePath} doesn't exists");
            } else if (!filePath.EndsWith (".json")) {
                _logger.LogError ($"File {filePath} doesn't have .json extension");
                throw new InvalidFileException ($"File {filePath} doesn't have .xml extension");
            }
            
            try
            {
                return new JsonSerializer()
                            .Deserialize<T>(
                                new JsonTextReader(
                                    new StreamReader(fileInfo.CreateReadStream())
                                    )
                                );

            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}