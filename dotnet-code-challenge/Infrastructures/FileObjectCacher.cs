using System;
using System.Collections.Generic;
using dotnet_code_challenge.Exceptions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;

namespace dotnet_code_challenge.Infrastructures
{
    public class FileObjectCacher : IFileObjectCacher
    {
        private readonly IFileProvider _fileProvider;
        private readonly ILogger<FileObjectCacher> _logger;
        private IDictionary<string, Tuple<DateTimeOffset, object>> _cache = new Dictionary<string, Tuple<DateTimeOffset, object>>();
        
        public FileObjectCacher(IFileProvider fileProvider, ILogger<FileObjectCacher> logger)
        {
            _fileProvider = fileProvider;
            _logger = logger;
        }

        public T GetCache<T>(string path)
        {                   
            var file = _fileProvider.GetFileInfo(path);
            if (!file.Exists)
            {
                _logger.LogError($"Could not find file {path}");
                throw new InvalidFileException($"File {path} does not exists");
            }

            if (!_cache.ContainsKey(path.ToUpper()))
            {
                _logger.LogDebug($"Could not found cache with path {path}");
                return default(T);
            }

            var cacheValue = _cache[path.ToUpper()];

            if (file.LastModified != cacheValue.Item1)
            {
                _logger.LogDebug($"File {path} has been modified, returning null");
                return default(T);
            }

            _logger.LogDebug($"Cache {path} found");
            return (T) cacheValue.Item2;                                       
        }

        public void SetCache<T>(string path, T obj)
        {
            var file = _fileProvider.GetFileInfo(path);
            if (!file.Exists)
                throw new InvalidFileException($"File {path} does not exists");

            var cacheValue = new Tuple<DateTimeOffset, object>(file.LastModified, obj);

            _cache[path.ToUpper()] = cacheValue;
        }
    }
}