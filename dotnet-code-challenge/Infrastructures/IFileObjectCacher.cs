
namespace dotnet_code_challenge.Infrastructures
{
    public interface IFileObjectCacher
    {
         T GetCache<T>(string path);
         void SetCache<T>(string path, T obj);
    }

}