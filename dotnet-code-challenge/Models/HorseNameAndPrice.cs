﻿namespace dotnet_code_challenge.Models
{
    public class HorseNameAndPrice
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
