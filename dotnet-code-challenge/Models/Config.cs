﻿namespace dotnet_code_challenge.Models
{
    public class Config
    {
        public string CaulfieldRaceFilePath { get; set; }
        public string WolferhamptonRaceFilePath { get; set; }
    }
}