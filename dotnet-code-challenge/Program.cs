﻿using System;
using System.Collections.Generic;
using System.IO;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Infrastructures;
using dotnet_code_challenge.Models;
using dotnet_code_challenge.Projectors;
using dotnet_code_challenge.Repositories;
using dotnet_code_challenge.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;

namespace dotnet_code_challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();
               
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection, config);
            var serviceProvider = serviceCollection.BuildServiceProvider();
            
            serviceProvider.GetService<App>().Run();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection, IConfigurationRoot config)
        {
            serviceCollection
                .AddOptions()
                .Configure<Config>(config.GetSection("ProviderConfig"))
                .AddLogging()
                .AddSingleton<IXmlDeserializer, XmlDeserializer>()
                .AddSingleton<IJsonDeserializer, JsonDeserializer>()
                .AddSingleton<Func<string, IFileDeserializer>>(x => (filename) =>
                {
                    var extension = filename.Substring(filename.LastIndexOf("."));
                    IFileDeserializer result = null;
                    switch (extension)
                    {
                        case ".xml":
                            result = x.GetService<IXmlDeserializer>();
                            break;
                        case ".json":
                            result = x.GetService<IJsonDeserializer>();
                            break;
                    }

                    return result;
                })
                .AddTransient<IHorseRepository, CaulfiedRaceRepository>()
                .AddTransient<IHorseRepository, WolferhamptonRaceRepository>()
                .AddTransient<IHorseService, HorseService>()
                .AddSingleton<IFactory<IEnumerable<HorseOrderByEnum>, IEnumerable<IProjector<Horse>>>, ProjectorFactory>()
                .AddTransient<IFileObjectCacher, FileObjectCacher>()
                .AddTransient<IFileProvider>(x => new PhysicalFileProvider(AppDomain.CurrentDomain.BaseDirectory))
                .AddTransient<App>();
        }
    }
}
