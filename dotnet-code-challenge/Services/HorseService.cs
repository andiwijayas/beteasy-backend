﻿using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Models;
using dotnet_code_challenge.Projectors;
using dotnet_code_challenge.Repositories;
using Microsoft.Extensions.Logging;

namespace dotnet_code_challenge.Services
{
    public class HorseService : IHorseService 
    {
        private readonly IEnumerable<IHorseRepository> _horseRepositories;
        private readonly IFactory<IEnumerable<HorseOrderByEnum>, IEnumerable<IProjector<Horse>>> _projectorFactory;
        private readonly ILogger<HorseService> _logger;

        public HorseService(
            IEnumerable<IHorseRepository> horseRepositories, 
            IFactory<IEnumerable<HorseOrderByEnum>, IEnumerable<IProjector<Horse>>> projectorFactory,
            ILogger<HorseService> logger)
        {
            _horseRepositories = horseRepositories;
            _projectorFactory = projectorFactory;
            _logger = logger;
        }
        
        public IEnumerable<Horse> GetHorses(IEnumerable<HorseOrderByEnum> orderBys)
        {            
            
            var list = _horseRepositories
                .Aggregate(
                    new List<Horse>(), 
                    (l, repo) => l.Concat(repo.GetHorses()).ToList()
                );
            
            var projectedList = _projectorFactory.Get(orderBys)
                .Aggregate(
                    list,
                    (output, projector) => projector.GetProjection(output).ToList()
                );

            return projectedList;

        }
    }
}