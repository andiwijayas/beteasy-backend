﻿using System;
using System.Collections.Generic;
using dotnet_code_challenge.Enums;

namespace dotnet_code_challenge.Services
{
    public interface IFactory<in TKey, out TObject>
    {
        TObject Get(IEnumerable<HorseOrderByEnum> key);
    }
}