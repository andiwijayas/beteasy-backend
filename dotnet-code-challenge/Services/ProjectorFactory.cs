﻿using System.Collections.Generic;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Models;
using dotnet_code_challenge.Projectors;

namespace dotnet_code_challenge.Services
{
    public class ProjectorFactory: IFactory<IEnumerable<HorseOrderByEnum>, IEnumerable<IProjector<Horse>>>
    {
        public IEnumerable<IProjector<Horse>> Get(IEnumerable<HorseOrderByEnum> key)
        {
            var list = new List<IProjector<Horse>>();
            foreach (var orderBy in key)
            {
                switch (orderBy)
                {
                    case HorseOrderByEnum.NameAscending:
                        list.Add(new OrderByNameProjectorAscending());
                        break;
                    case HorseOrderByEnum.PriceAscending:
                        list.Add(new OrderByPriceProjectorAscending());
                        break;
                }
            }
            return list;
        }
    }
}