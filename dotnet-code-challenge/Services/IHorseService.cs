﻿using System.Collections.Generic;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Models;

namespace dotnet_code_challenge.Services
{
    public interface IHorseService
    {
        IEnumerable<Horse> GetHorses(IEnumerable<HorseOrderByEnum> orderBys);
    }
}