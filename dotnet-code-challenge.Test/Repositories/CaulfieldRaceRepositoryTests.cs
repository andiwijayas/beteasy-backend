﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using DeepEqual.Syntax;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Infrastructures;
using dotnet_code_challenge.Models;
using dotnet_code_challenge.Repositories;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using Shouldly;
using TestStack.BDDfy;
using Xunit;

namespace dotnet_code_challenge.Test.Repositories
{
    public class CaulfieldRaceRepositoryTests
    {
        private Func<string, IFileDeserializer> _deserializerFactory;
        private IOptions<Config> _config;
        private IFileObjectCacher _fileCache;
        private ILogger<CaulfiedRaceRepository> _logger;
        private CaulfiedRaceRepository _caulfieldRepo;
        private IEnumerable<Horse> _horses;

        public CaulfieldRaceRepositoryTests()
        {
            _deserializerFactory = Substitute.For<Func<string, IFileDeserializer>>();
            _config = Substitute.For<IOptions<Config>>();
            _fileCache = Substitute.For<IFileObjectCacher>();
            _logger = Substitute.For<ILogger<CaulfiedRaceRepository>>();
            _caulfieldRepo = new CaulfiedRaceRepository(_deserializerFactory, _config, _fileCache, _logger);
        }

        [Fact]
        public void WhenGetRaceIsCalled_ItShouldRetrieveValueFromCacheFirst()
        {
            var config = new Config {CaulfieldRaceFilePath = "blah"};
            this.Given(x => x.GivenConfiguration(config))
                .When(x => x.GetRaceIsCalled())
                .Then(x => x.ThenGetCacheShouldBeCalledWithParameter(config.CaulfieldRaceFilePath))
                .BDDfy();
        }
        
        [Fact]
        public void WhenGetRaceIsCalled_ItShouldSaveValueToCacheWhenCacheReturnsNull()
        {
            var config = new Config {CaulfieldRaceFilePath = "blah"};
            var testObject = new Fixture().Create<CaulfieldRace.Meeting>();
            var deserializer = Substitute.For<IFileDeserializer>();
            deserializer
                .Deserialize<CaulfieldRace.Meeting>(config.CaulfieldRaceFilePath)
                .Returns(testObject);
            
            this.Given(x => x.GivenConfiguration(config))
                .And(x => x.GivenCacheReturns(null))
                .And(x => x.GivenDeserializeFactoryReturns(deserializer))
                .When(x => x.GetRaceIsCalled())
                .Then(x => x.ThenSetCacheShouldBeCalledWithParameter(config.CaulfieldRaceFilePath, testObject))
                .BDDfy();
        }
        
        [Fact(Skip = "not enough time, test object too complex")]
        public void WhenGetHorsesIsCalled_ItShouldReturnTheListOfHorses()
        {
            var config = new Config {CaulfieldRaceFilePath = "blah"};
            var testObject = new Fixture().Create<CaulfieldRace.Meeting>();
            var horses = testObject.Races.Race.Horses.Horse;
            var horsesPrices = testObject.Races.Race.Prices.Price.Horses.Horse
                .Select(x => new Horse
                {
                    Name = x.Name ?? horses.FirstOrDefault(y => y._Number == x.Id).Name,
                    Price = Convert.ToDecimal(x.Price ?? "0"),
                    RaceName = Race.Caulfied
                }); 
                                    
            var deserializer = Substitute.For<IFileDeserializer>();
            deserializer
                .Deserialize<CaulfieldRace.Meeting>(config.CaulfieldRaceFilePath)
                .Returns(testObject);
            
            this.Given(x => x.GivenConfiguration(config))
                .And(x => x.GivenCacheReturns(testObject))
                .When(x => x.GetHorsesIsCalled())
                .Then(x => x.ThenItShouldReturnAListOfHorses(horsesPrices))
                .BDDfy();
        }

        private void ThenItShouldReturnAListOfHorses(IEnumerable<Horse> expected)
        {
            _horses.IsDeepEqual(expected).ShouldBeTrue();
        }

        private void GetHorsesIsCalled()
        {
            _horses = _caulfieldRepo.GetHorses();
        }
        
        private void ThenSetCacheShouldBeCalledWithParameter(string path, CaulfieldRace.Meeting testObject)
        {
            _fileCache.Received().SetCache(path, testObject);
        }

        private void GivenDeserializeFactoryReturns(object deserializer)
        {
            _deserializerFactory.Invoke(Arg.Any<string>()).Returns(deserializer);
        }

        private void GivenCacheReturns(object obj)
        {
            _fileCache
                .GetCache<CaulfieldRace.Meeting>(Arg.Any<string>())
                .Returns(obj);
        }


        private void GivenConfiguration(Config config)
        {
            _config.Value.Returns(config);
        }

        private void GetRaceIsCalled()
        {
            _caulfieldRepo.GetRace();
        }

        private void ThenGetCacheShouldBeCalledWithParameter(string path)
        {
            _fileCache.Received().GetCache<CaulfieldRace.Meeting>(Arg.Is<string>(x => x == path));
        }
    }
}