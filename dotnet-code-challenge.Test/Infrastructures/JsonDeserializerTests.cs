using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Xunit;
using TestStack.BDDfy;
using Shouldly;
using System;
using System.IO;
using DeepEqual.Syntax;
using dotnet_code_challenge.Infrastructures;
using dotnet_code_challenge.Exceptions;

namespace dotnet_code_challenge.Test.Infrastructures
{
    public class JsonDeserializerTests
    {
        private readonly IFileProvider _fileProvider;
        private readonly ILogger<JsonDeserializer> _logger;
        private readonly JsonDeserializer _jsonDeserializer;
        private Exception _exception;
        private object _result;

        public JsonDeserializerTests()
        {
            _fileProvider = Substitute.For<IFileProvider>();
            _logger = Substitute.For<ILogger<JsonDeserializer>>();
            _jsonDeserializer = new JsonDeserializer(_logger, _fileProvider);            
        }

        [Fact]
        public void WhenParameterIsNull_ItShouldThrowInvalidFileException()
        {
            this.Given(x=> GivenFileExists(null))
                .When(x => WhenDeserializeIsCalledWith(null))
                .Then(x => ThenInvalidFileExceptionIsThrown())
                .BDDfy();
        }

        [Fact]
        public void WhenFileNotExists_ItShouldThrowInvalidFileException()
        {
            var fileInfo = Substitute.For<IFileInfo>();
            fileInfo.Exists.Returns(false);
            this.Given(x=> GivenFileExists(fileInfo))
                .When(x => WhenDeserializeIsCalledWith("blah"))
                .Then(x => ThenInvalidFileExceptionIsThrown())
                .BDDfy();
        }

        [Fact]
        public void WhenFileDoesNotHaveJsonExtension_ItShouldThrowInvalidFileException()
        {
            var fileInfo = Substitute.For<IFileInfo>();
            fileInfo.Exists.Returns(true);
            this.Given(x=> GivenFileExists(fileInfo))
                .When(x => WhenDeserializeIsCalledWith("blah.xml"))
                .Then(x => ThenInvalidFileExceptionIsThrown())
                .BDDfy();
        }

        [Fact]
        public void WhenPassedWithAValidJsonFile_ItShouldReturnValidObject()
        {
            var fileInfo = Substitute.For<IFileInfo>();
            fileInfo.Exists.Returns(true);
            fileInfo
                .CreateReadStream()
                .Returns(GenerateStreamFromString("{'test1': 'hello', 'test2':'world'}"));
            
            this.Given(x=> GivenFileExists(fileInfo))
                .When(x => WhenDeserializeIsCalledWith("blah.json"))
                .Then(x => ThenResultShouldMatch(new Test {test1 = "hello", test2 = "world"}))
                .BDDfy();
        }


        private void GivenFileExists(IFileInfo info)
        {
            _fileProvider.GetFileInfo(Arg.Any<string>()).Returns(info);
        }

        private void WhenDeserializeIsCalledWith(string path)
        {
            try 
            {
                _result = _jsonDeserializer.Deserialize<Test>(path);
            } 
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        private void ThenInvalidFileExceptionIsThrown()
        {
            _exception.ShouldBeOfType(typeof(InvalidFileException));
        }

        private void ThenResultShouldMatch(object expected)
        {
            _result.IsDeepEqual(expected).ShouldBeTrue();
        }

        private static Stream GenerateStreamFromString(string s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public class Test
        {
            public string test1 { get; set; }
            public string test2 { get; set; }
        }
    }
}