﻿using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using dotnet_code_challenge.Enums;
using dotnet_code_challenge.Models;
using dotnet_code_challenge.Projectors;
using dotnet_code_challenge.Repositories;
using dotnet_code_challenge.Services;
using Microsoft.Extensions.Logging;
using NSubstitute;
using Shouldly;
using TestStack.BDDfy;
using Xunit;

namespace dotnet_code_challenge.Test.Services
{
    public class HorseServiceTests
    {
        private IEnumerable<IHorseRepository> _horseRepositories;
        private IFactory<IEnumerable<HorseOrderByEnum>, IEnumerable<IProjector<Horse>>> _projectorFactory;
        private HorseService _horseService;
        private IEnumerable<Horse> _returnValue;
        private ILogger<HorseService> _logger;

        public HorseServiceTests()
        {
            _horseRepositories = Substitute.For<IEnumerable<IHorseRepository>>();
            _projectorFactory = Substitute.For<IFactory<IEnumerable<HorseOrderByEnum>, IEnumerable<IProjector<Horse>>>>();
            _logger = Substitute.For<ILogger<HorseService>>();
            _horseService = new HorseService(_horseRepositories, _projectorFactory, _logger);
        }
        
        [Fact]
        public void WhenGetHorsesIsCalledWithPricingProjector_ShouldReturnArraySortedByPrice()
        {
            var fixture = new Fixture {RepeatCount = 5};
            var horses = fixture.Create<IList<Horse>>();

            this.Given(x => x.GivenRepositories(BuildHorseRepositories(horses, 3, 2)))
                .And(x => x.GivenProjectors(new List<IProjector<Horse>> {BuildSortByPriceProjector()}))
                .When(x => x.WhenGetHorsesIsCalledWithParameter(
                    new List<HorseOrderByEnum> {HorseOrderByEnum.PriceAscending})
                )
                .Then(x => x.ThenItMatchWithExpectedValue(horses.OrderBy(y => y.Price)))
                .BDDfy();
        }

        [Fact]
        public void WhenGetHorsesIsCalledWithNameProjector_ShouldReturnArraySortedByName()
        {
            var fixture = new Fixture {RepeatCount = 5};
            var horses = fixture.Create<IList<Horse>>();

            this.Given(x => x.GivenRepositories(BuildHorseRepositories(horses, 3, 2)))
                .And(x => x.GivenProjectors(new List<IProjector<Horse>> {BuildSortByNameProjector()}))
                .When(x => x.WhenGetHorsesIsCalledWithParameter(
                    new List<HorseOrderByEnum> {HorseOrderByEnum.NameAscending})
                )
                .Then(x => x.ThenItMatchWithExpectedValue(horses.OrderBy(y => y.Name)))
                .BDDfy();
        }

        private void ThenItMatchWithExpectedValue(IOrderedEnumerable<Horse> expected)
        {
            _returnValue.ShouldBe(expected.ToList());
        }

        private void WhenGetHorsesIsCalledWithParameter(List<HorseOrderByEnum> horseOrderByEnums)
        {
            _returnValue = _horseService.GetHorses(horseOrderByEnums);
        }


        private void GivenRepositories(IEnumerable<IHorseRepository> horseRepositories)
        {
            _horseRepositories.GetEnumerator().Returns(horseRepositories.GetEnumerator());
        }
        
        private void GivenProjectors(List<IProjector<Horse>> projectors)
        {
            _projectorFactory
                .Get(Arg.Any<IEnumerable<HorseOrderByEnum>>())
                .Returns(projectors);
        }

        private IProjector<Horse> BuildSortByPriceProjector()
        {
            var mockedProjector = Substitute.For<IProjector<Horse>>();
            mockedProjector
                .GetProjection(Arg.Any<IEnumerable<Horse>>())
                .Returns(x => ((IList<Horse>) x[0]).OrderBy(y => y.Price).ToList());
            return mockedProjector;
        }

        private IProjector<Horse> BuildSortByNameProjector()
        {
            var mockedProjector = Substitute.For<IProjector<Horse>>();
            mockedProjector
                .GetProjection(Arg.Any<IEnumerable<Horse>>())
                .Returns(x => ((IList<Horse>) x[0]).OrderBy(y => y.Name).ToList());
            return mockedProjector;
        }

        private IEnumerable<IHorseRepository> BuildHorseRepositories(IList<Horse> horses, params int[] arrayLength)
        {
            int fromIndex = 0, length = 0;
            
            for (var i = 0; i < arrayLength.Length; i++)
            { 
                fromIndex = (i == 0) 
                    ? 0 
                    : fromIndex + length;

                if (fromIndex == horses.Count) break;
                
                length = (fromIndex + arrayLength[i] > horses.Count) 
                    ? arrayLength[i] - horses.Count 
                    : arrayLength[i];
                
                
                var list = horses.Skip(fromIndex).Take(length).ToList();
                var mockedRepo = Substitute.For<IHorseRepository>();
                mockedRepo.GetHorses().Returns(list);
                yield return mockedRepo;
            }    
        }

    }
}