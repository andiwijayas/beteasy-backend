FROM microsoft/dotnet:2.0-runtime AS base
WORKDIR /app

FROM microsoft/dotnet:2.0-sdk AS build
WORKDIR /src
COPY ["dotnet-code-challenge/dotnet-code-challenge.csproj", "dotnet-code-challenge/"]
RUN dotnet restore "dotnet-code-challenge/dotnet-code-challenge.csproj"
COPY . .
WORKDIR "/src/dotnet-code-challenge"
RUN dotnet build "dotnet-code-challenge.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "dotnet-code-challenge.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "dotnet-code-challenge.dll"]
