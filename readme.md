# .NET Code Challenge

## To run application 

* `dotnet build`
* `cd dotnet-code-challenge`
* `dotnet run --project dotnet-code-challenge\dotnet-code-challenge.csproj`

## To run test

* From the root folder run `dotnet test`

This application is build with Onion Architecture in mind:
https://www.codeguru.com/csharp/csharp/cs_misc/designtechniques/understanding-onion-architecture.html
